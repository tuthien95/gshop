﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GShopBus.Bus;

namespace GShop.Controllers
{
    public class SearchController : Controller
    {
        //
        // GET: /Search/
        [HttpPost]
        public ActionResult Index(FormCollection f, int? page)
        {
            if (page == null)
            {
                page = 1;
            }
            string sKey = f.Get("Search").ToString();
            return View(ProductBus.Search(sKey, page));
        }
    }
}