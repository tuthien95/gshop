﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PetaPoco;
using GShopConnection;
using GShopBus.Bus;

namespace GShop.Areas.Admin.Controllers
{
    public class AdminProductController : Controller
    {
        //
        // GET: /Admin/AdminProduct/
        public ActionResult Index(int? page)
        {
            if (page == null)
            {
                page = 1;
            }
            return View(ProductBus.List(page));            
        }

        //
        // GET: /Admin/AdminProduct/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Admin/AdminProduct/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Admin/AdminProduct/Create
        [HttpPost]
        public ActionResult Create(product p)
        {
            try
            {
                // TODO: Add insert logic here
                ProductBus.Add(p);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Admin/AdminProduct/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Admin/AdminProduct/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Admin/AdminProduct/Delete/5        
        public ActionResult Delete(int id)
        {
            ProductBus.Remove(id);
                return RedirectToAction("Index");
        }
        
        //
        // POST: /Admin/AdminProduct/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
