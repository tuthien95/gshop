﻿using System.ComponentModel.DataAnnotations;
using GShopConnection;

namespace GShop.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }
    }

    public class ManageUserViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel : user
    {
        [Required]
        [Display(Name = "User name")]
        public new string f_Username {
            get
            {
                return base.f_Username;
            }
            set
            {
                base.f_Username = value;
            }
        }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public new string f_Password {
            get
            {
                return base.f_Password;
            }
            set
            {
                base.f_Password = value;
            }
        }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("f_Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Full Name")]
        public new string f_Name
        {
            get
            {
                return base.f_Name;
            }
            set
            {
                base.f_Name = value;
            }
        }


        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public new string f_Email
        {
            get
            {
                return base.f_Email;
            }
            set
            {
                base.f_Email = value;
            }
        }


        [Required]
        [Display(Name = "Birthday")]
        public new System.DateTime? f_DOB
        {
            get
            {
                return base.f_DOB;
            }
            set
            {
                base.f_DOB = value;
            }
        }
    }
}
