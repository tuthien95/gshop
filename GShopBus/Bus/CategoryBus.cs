﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GShopConnection;

namespace GShopBus.Bus
{
    public class CategoryBus
    {
        public static IEnumerable<category> List()
        {
            return new GShopConnectionDB().Query<category>("Select * from categories");
        }
        public static void Add(category cat)
        {
            var db = new GShopConnectionDB();
            db.Insert("categories", cat);
        }
        //public static void Xoa(ChuDe cd)
        //{
        //    var db = new BookShopConnectionDB();
        //    db.Delete("ChuDe", "MaChuDe", cd);
        //}
        //public static void XoaCD(int id)
        //{
        //    string sql = string.Format("DELETE FROM ChuDe WHERE MaChuDe={0}", id);
        //    new BookShopConnectionDB().Execute(sql);
        //}
        //public static void sua(ChuDe cd)
        //{
        //    var db = new BookShopConnectionDB();
        //    db.Update("ChuDe", "MaChuDe", cd);
        //}
        //public static ChuDe LayChuDe(int id)
        //{
        //    var db = new BookShopConnectionDB();
        //    IEnumerable<ChuDe> ds = db.Query<ChuDe>("Select * From ChuDe Where MaChuDe =" + id);
        //    return ds.ElementAt(0);
        //}
        //public static ChuDe GetById(int id)
        //{
        //    var db = new BookShopConnectionDB();
        //    return db.SingleOrDefault<ChuDe>("select * from ChuDe where MaChuDe=@0", id);
        //}
    }
}
