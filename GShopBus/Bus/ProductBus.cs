﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GShopConnection;
using PetaPoco;

namespace GShopBus.Bus
{
    public class ProductBus
    {
        public static IEnumerable<product> List()
        {
            var db = new GShopConnectionDB();
            return db.Query<product>("SELECT * FROM products");
        }
        public static Page<product> List(int? page)
        {
            GShopConnectionDB db = new GShopConnectionDB();
            return db.Page<product>(page.Value, 4, "SELECT * FROM products");
        }
        public static Page<product> Search(string key, int? page)
        {
            GShopConnectionDB db = new GShopConnectionDB();
            Page<product> sp = db.Page<product>(page.Value, 6, "select * from products where ProName like '%' + N'" + key + "' + '%' COLLATE SQL_Latin1_General_CP1_CI_AS");
            return sp;
        }
        public static product GetById(int id)
        {
            var db = new GShopConnectionDB();
            return db.SingleOrDefault<product>("SELECT p.ProID, p.ProName, p.Price, b.BraName FROM products p, brands b where p.ProID=@0 and p.BraID = b.BraID", id);
        }
        public static IEnumerable<product> LoadByCatId(int id)
        {
            var db = new GShopConnectionDB();
            return db.Query<product>("SELECT * FROM products where CatID=@0", id);
        }
        public static IEnumerable<product> LoadByBraId(int id)
        {
            var db = new GShopConnectionDB();
            return db.Query<product>("SELECT * FROM products where BraID=@0", id);
        }
        public static product Get(int id)
        {
            var db = new GShopConnectionDB();
            IEnumerable<product> ds = db.Query<product>("SELECT * FROM products WHERE ProID =" + id);
            return ds.ElementAt(0);
        }
        public static void Add(product products)
        {
            var db = new GShopConnectionDB();
            db.Insert("products", products);
        }
        public static void Remove(int id)
        {
            var db = new GShopConnectionDB();
            db.Delete<product>("WHERE ProID = @0", id);
        }
        public static void Edit(product products)
        {
            //var db = new GShopConnectionDB();

            GShopConnectionDB db = new GShopConnectionDB();
            db.Update("products", "ProID", products);
        }
        public static product ChiTiet(int id)
        {
            var db = new GShopConnectionDB();
            IEnumerable<product> s = db.Query<product>("SELECT * FROM products Where ProID =" + id);
            return s.ElementAt(0);
        }

        /*
        public static void TacGia()
        {
            string sql = string.Format("select tg.TenTacGia,s.Ten products from products s, TacGia tg where(s.MaTacGia=tg.MaTacGia)");
            new GShopConnectionDB().Execute(sql);
        }
        public static IEnumerable<> LoadChuDe()
        {
            var db = new GShopConnectionDB();
            return db.Query<ChuDe>("Select MaChuDe,TenChuDe From ChuDe ");
        }
        public static IEnumerable<NhaXuatBan> LoadNXB()
        {
            var db = new GShopConnectionDB();
            return db.Query<NhaXuatBan>("Select MaNXB,TenNXB from NhaXuatBan");
        }
        
        public static IEnumerable<product> productsMoiNhat()
        {
            var db = new GShopConnectionDB();
            return db.Query<product>("Select Top 10 * From products Order By NgayCapNhat DESC");
        }
        public static IEnumerable<product> SoLuotXem()
        {
            var db = new GShopConnectionDB();
            return db.Query<product>("Select Top 10 * From products Order By SoLuotXem DESC");
        }
        public static IEnumerable<product> SoLuotBan()
        {
            var db = new GShopConnectionDB();
            return db.Query<product>("Select Top 10 * From products Order By SoLuotBan DESC");
        }
        */
    }
}
