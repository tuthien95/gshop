﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GShopConnection;

namespace GShopBus.Bus
{
    public class BrandBus
    {
        public static IEnumerable<brand> List()
        {
            return new GShopConnectionDB().Query<brand>("Select * from brands");
        }
    }
}
