﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GShopConnection;

namespace GShopBus.Bus
{
    public class UserBus
    {
        public static void Insert(user u)
        {
            user a = new user();
            a.f_ID = u.f_ID;
            a.f_Username = u.f_Username;
            a.f_Password = u.f_Password;
            a.f_Name = u.f_Name;
            a.f_Email = u.f_Email;
            a.f_DOB = u.f_DOB;
            a.f_Permission = u.f_Permission;
            
            var db = new GShopConnectionDB();
            db.Insert("users", a);
        }
    }
}
